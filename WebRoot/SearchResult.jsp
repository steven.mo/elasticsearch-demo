<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fun"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>领航搜索-为您从互联网中精选“${wd}”的相关内容！</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
	
		<link rel="stylesheet" type="text/css"
			href="${pageContext.request.contextPath}/css/list.css" />
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/js/jquery-1.6.2.min.js"></script>
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/js/jquery.form.js"></script>
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/js/pager-taglib.js"></script>
			
		
		<script type="text/javascript">
			(function() {
			    var $backToTopTxt = "返回顶部", $backToTopEle = $('<div class="backToTop"></div>').appendTo($("body"))
			        .text($backToTopTxt).attr("title", $backToTopTxt).click(function() {
			            $("html, body").animate({ scrollTop: 0 }, 120);
			    }), $backToTopFun = function() {
			        var st = $(document).scrollTop(), winh = $(window).height();
			        (st > 0)? $backToTopEle.show(): $backToTopEle.hide();    
			        //IE6下的定位
			        if (!window.XMLHttpRequest) {
			            $backToTopEle.css("top", st + winh - 166);    
			        }
			    };
			    $(window).bind("scroll", $backToTopFun);
			    $(function() { $backToTopFun(); });
			})();
			</script>
	</head>  
	<body>
	    <!-- JiaThis Button BEGIN -->
<script type="text/javascript" src="http://v2.jiathis.com/code/jiathis_r.js?move=0&amp;btn=r1.gif" charset="utf-8"></script>
<!-- JiaThis Button END -->
	  <!-- <div style="height:900px;"></div>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/scrolltopcontrol.js"></script>  -->
		<div id="s_page">
			<jsp:include page="head.jsp"></jsp:include>
			<div id="s_main">
				<div class="nav">
					<div id="fh" class="n_l">
						<span>以下呈现的是“${wd}”的精选结果</span>
					</div>
					<div class="n_r">
						共检索到数据约<span style="color: red">${total}</span>条,耗时<span style="color: red">${spendTime}</span>秒
					</div>
				</div>
				<table width="100%" cellspacing="0" cellpadding="0" id="main">
					<tr>
						<td class="s_l">
							<ol>
							<!-- ${fun:length(pageList)} -->
								<!--循环遍历数据-->
								<c:forEach var="pojo" varStatus="s" items="${pageList}">
								
									<li class="topsp">
										<h3>
											<a href="${pojo.links}" target="_blank"> ${pojo.title}</a>
										</h3>
										<div class="result_summary">
											<div class="url">
												<cite>
											    <%--  ${pojo.content} --%>
											  ${fun:substring(pojo.content,0,fun:indexOf(pojo.content, wd) +50 )}
											</cite>
											</div>
										</div>
									</li>
								</c:forEach>
							</ol>
							<form action="solrSearch" method="get">
							<input type="hidden" name="wd" value="${wd}"/>
							<div id="page">
								<div class="pg">
									<c:if test="${pager!=null}">
										<pg:pager id="pager" maxPageItems="${pager.maxPageItems}"
											items="${total}" export="offset,currentPageNumber=pageNumber">
											<pg:index export="items,pages">
												<pg:first>
													<a href="javascript:doPostBack('pager',1)"><nobr>
															首页
														</nobr> </a>
												</pg:first>
												<pg:prev export="first">
													<a href="javascript:doPostBack('pager',${first})">前一页</a>
												</pg:prev>
												<pg:pages export="pageNumber,first">
													<c:choose>
														<c:when test="${currentPageNumber eq pageNumber}">
															<font class="current">${pageNumber }</font>
														</c:when>
														<c:otherwise>
															<a href="javascript:doPostBack('pager',${first})">${pageNumber}</a>
														</c:otherwise>
													</c:choose>
												</pg:pages>
												<pg:next export="first">
													<a href="javascript:doPostBack('pager',${first})">下一页</a>
												</pg:next>
												<pg:last export="first">
													<a href="javascript:doPostBack('pager',${first})"><nobr>
															尾页
														</nobr> </a>
												</pg:last>
												<pg:skip pages="${pageNumber}">
													<input name="pager.offset" type="hidden" value="0" />
													<input type="text" name="pager.pageNumber"
														number="${pageNumber}" value="${pageNumber}"
														pages="${pages}" style="display: none"></input>
													<input name="pager.submit" type="submit" style="display: none" value=""
														onclick="return PT_CheckSubmit('pager',2)" />
													<input type="hidden" name="pager.maxPageItems" value="10"/>
												</pg:skip>
											</pg:index>
										</pg:pager>
									</c:if>
								</div>
							</div>
							</form>
							<div class="k">
							</div>
						</td>
						<!--right side-->
						<td class="s_r">
							<div class="borl">
								<div class="c1" ss_c="w.r.fav">
									<h4>
										<a href="mailto:1215157376@qq.com">广告招租</a>
									</h4>
									<ol class="content">
									</ol>
								</div>
							</div>
						</td>
					</tr>
					<%--
					<tr>
						<td colspan="2">
							<div class="tj">
								<div ss_c="search.hint">
									<table cellpadding="0" cellspacing="0">
										<tr>
											<th valign="top" rowspan="2">
												<b>推荐搜索：</b>
											</th>
											<asp:Repeater ID="rp_wd1" runat="server">
												<ItemTemplate>
												<td valign="top">
													<a href="#"> Container.DataItem</a>
												</td>
										</tr>
										<tr>
											<asp:Repeater ID="rp_wd2" runat="server">

												<td valign="top">
													<a href="#"> #Container.DataItem </a>
												</td>
												</ItemTemplate>
										</tr>
									</table>
								</div>
							</div>
						</td>
					</tr>
				--%>
				</table>
				<div class="footer_search">
					<form action="solrSearch" method="get">
						<input class="search_input" type="text" name="wd" value="${wd}" />
						<input value="搜 索" class="search_button" type="submit" />
					</form>
				</div>
			</div>
			<jsp:include page="foot.jsp"></jsp:include>
		</div>
	</body>
</html>
