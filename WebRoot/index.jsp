<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
	<head>
		<title>草履虫搜索引擎</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/js/jquery-1.6.2.min.js"></script>
	<script type="text/javascript">
        var line = 0;
       
        function del(){
            if($("#newDiv")){
                $("#newDiv").remove();
                line = 0;
            }
        }
   
        $(document).ready(function(){
            //文本框失去焦点时层消失
            $(document.body).click(function(){
                del();
            });
           
            $(document).keydown(function(){
                // 38 上  40下 13 回车
                if($("#newDiv")){
                    if(event.keyCode == 40){
                        $("table tbody tr").eq(line)
                            .css("backgroundColor", "blue")
                            .css("color", "white");
                        $("table tbody tr").eq(line < 0 ? 0 : line - 1)
                            .css("backgroundColor", "white")
                            .css("color", "black");
                        line = (line == $("table tbody tr").length ? 0 : line + 1);
                    }else if(event.keyCode == 38){
                        line = (line == 0 ? $("table tbody tr").length : line - 1);
                        $("table tbody tr").eq(line)
                            .css("backgroundColor", "blue")
                            .css("color", "white");
                        $("table tbody tr").eq(line > $("table tbody tr").length ? 0 : line + 1)
                            .css("backgroundColor", "white")
                            .css("color", "black");
                    }else if(event.keyCode == 13){
                        $("#key").val($("table tbody tr").eq(line - 1).find("td").eq(0).html());
                        del();
                    }
                }   
            });
       
            $("#key").bind("propertychange", function(){
                del();
           
                var top = $("#key").offset().top;
                var left = $("#key").offset().left;
                var newDiv = $("<div/>").width($("#key").width() + 6)
                    .css("position", "absolute")
                    .css("backgroundColor", "white")
                    .css("left", left)
                    .css("top", top + $("#key").height() + 6)
                    .css("border", "1px solid blue")
                    .attr("id", "newDiv");
                   
                var table = $("<table width='100%'/>")
                    .attr("cellpadding", "0")
                    .attr("cellspacing", "0");
               
                $.getJSON("GoogleServlet", {key: $("#key").val()}, function(json){

                    alert(json);
                    $(json).find("results result").each(function(){
                        var key = $(this).attr("key");
                        var count = $(this).attr("count");
                       
                        var tr = $("<tr/>").css("cursor", "pointer").mouseout(function(){
                            $(this).css("backgroundColor", "white").css("color", "black");
                        }).mouseover(function(){
                            $(this).css("backgroundColor", "blue").css("color", "white");
                        }).click(function(){
                            $("#key").val($(this).find("td").eq(0).html());
                           
                            del();
                        });
                        var td1 = $("<td/>").html(key).css("fontSize", "12px")
                            .css("margin", "5 5 5 5");
                        var td2 = $("<td/>").html("共有" + count + "个结果")
                            .attr("align", "right").css("fontSize", "12px")
                            .css("color", "green").css("margin", "5 5 5 5");
                       
                        tr.append(td1).append(td2);
                        table.append(tr);
                        newDiv.append(table);
                    });
                });
               
                $(document.body).append(newDiv);
               
                if($("#key").val() == ""){
                    $("#newDiv").remove();
                }               
            });
        });
    </script>
  </head>
 
  <body>
    <h1>Google搜索</h1>
    <div style="margin-top: 20px; margin-left: 30px">
        请输入搜索关键字:<input name="key" id="key" style="width: 300">
    <input type="button" value="Goolge一下">
    </div>
  </body>
</html>

	