<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>使用solr检索数据测试</title>
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/js/jquery-1.6.2.min.js"></script>
		<script type="text/javascript"
			src="${pageContext.request.contextPath}/js/jquery.form.js"></script>
			<script type="text/javascript"
			src="${pageContext.request.contextPath}/js/pager-taglib.js"></script>
<script type="text/javascript">
<%--每次表单提交都是无刷新的--%>
// wait for the DOM to be loaded
$(document).ready(function() { 
    $('#solrSearch').ajaxForm(//通过ajax方式提交表单数据	
    	function(responseText, statusText){
			var bodyStart=responseText.lastIndexOf('<div id="ajaxContent">');//重新构建form表单中的数据
			var htmlStr = responseText.substr(bodyStart);
			htmlStr=htmlStr.substring(0,htmlStr.lastIndexOf('</form>'));
			$('#ajaxContent').html(htmlStr);
		}
    ); 
}); 
</script>
	</head>
	<body>
		<s:form action="solrSearch" id="solrSearch" name="solrSearch"
			theme="simple" method="post">
			<div>请输入检索关键字：<input name="key" id="key" type="text"/>&nbsp;&nbsp;&nbsp;<input type="button" value="搜索" onclick="$('#solrSearch').submit();"  /></div><br>
		
			<div id="ajaxContent">
	<c:if test="${pager!=null}">
			<div>共检索到数据<span style="color:red">${total}</span>条,耗时<span style="color:red">${spendTime}</span>秒</div><br>
			</c:if>
				<table>
					<tr>
						<td>
							编号
						</td>
						<td>
							姓名
						</td>
						<td>
							标题
						</td>
						<td>
							单价
						</td>
						<td>
							主题
						</td>
					</tr>
	
					<c:forEach var="pojo" varStatus="s" items="${pageList}">
					<tr>
							<td>
								${pojo.id}
							</td>
							<td>
								${pojo.name}
							</td>
							<td>
								${pojo.title}
							</td>
							<td>
								${pojo.price}
							</td>
							<td>
								${pojo.simple}
							</td>
						</tr>
					</c:forEach>
				</table>


<c:if test="${pager!=null}">
				<pg:pager id="pager" maxPageItems="${pager.maxPageItems}"
					items="${total}" export="offset,currentPageNumber=pageNumber">
					<pg:index export="items,pages">
						<pg:page>
							<span>第${pageNumber}/${pages}页,共${items}记录 </span>
						</pg:page>
						<pg:first>
							<a href="javascript:doPostBack('pager',1)"><nobr>
									[首页]
								</nobr>
							</a>
						</pg:first>
						<pg:prev export="first">
							<a href="javascript:doPostBack('pager',${first})">前一页</a>
						</pg:prev>
						<pg:pages export="pageNumber,first">
							<c:choose>
								<c:when test="${currentPageNumber eq pageNumber}">
									<font color="red">${pageNumber }</font>
								</c:when>
								<c:otherwise>
									<a href="javascript:doPostBack('pager',${first})">${pageNumber}</a>
								</c:otherwise>
							</c:choose>
						</pg:pages>
						<pg:next export="first">
							<a href="javascript:doPostBack('pager',${first})">下一页</a>
						</pg:next>
						<pg:last export="first">
							<a href="javascript:doPostBack('pager',${first})"><nobr>
									[尾页]
								</nobr>
							</a>
						</pg:last>
						<pg:skip pages="${pageNumber}">
							<input name="pager.offset" type="hidden" value="0" />
							<input type="text" name="pager.pageNumber"
								number="${pageNumber}" value="${pageNumber}" pages="${pages}"></input>
							<input name="pager.submit" type="submit" value="转到"
								onclick="return PT_CheckSubmit('pager',2)" />
							<span>每页</span>
							<s:select name="pager.maxPageItems" list="#{10:10,20:20,30:30}"
								onchange="doPostBack('pager','maxPageItems')">
							</s:select>
							<span>条</span>
						</pg:skip>
					</pg:index>
				</pg:pager>
				</c:if>
			</div>
		</s:form>
	</body>
</html>