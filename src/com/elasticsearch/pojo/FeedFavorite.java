// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   FeedFavorite.java

package com.elasticsearch.pojo;

import java.io.Serializable;
import java.util.Date;

// Referenced classes of package domain:
//            PageDomain

public class FeedFavorite implements Serializable {

	public FeedFavorite() {
	}

	private static final long serialVersionUID = 0x838b4de8b11b37a8L;
	private Integer id;
	private String content;
	private String title;

	private Date create_time;
	
	private String links;

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public String getLinks() {
		return links;
	}

	public void setLinks(String links) {
		this.links = links;
	}

}
